<?php
include_once ("../../vendor/autoload.php");

use Pondit\Calculator\AreaCalculator\Circle;
use Pondit\Calculator\AreaCalculator\Rectangle;
use Pondit\Calculator\AreaCalculator\Square;
use Pondit\Calculator\AreaCalculator\Triangle;

$circle = new Circle();
$circle->setCircle(50);

$rectangle = new Rectangle();
$rectangle->setRectangle();

$square = new Square(5,5);
//$square->setSquare();

$triangle = new Triangle();
$triangle->setTriangle();


?>