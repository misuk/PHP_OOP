<?php
include_once ("../../vendor/autoload.php");

use Pondit\Library\Book;
use Pondit\Library\Author;
use Pondit\Library\Publisher;

$book = new Book();
$author = new Author();
$publisher = new Publisher();
?>