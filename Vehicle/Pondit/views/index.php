<?php
include_once ("../vendor/autoload.php");

use Pondit\Vehicle\AirVehicle\Helicopter;
use Pondit\Vehicle\AirVehicle\Plane;

use Pondit\Vehicle\LandVehicle\Car;
use Pondit\Vehicle\LandVehicle\Truck;

use Pondit\Vehicle\WaterVehicle\Ship;




$helicopter = new Helicopter();
$plane = new Plane();
$car = new Car();
$truck = new Truck();
$ship = new Ship();
?>