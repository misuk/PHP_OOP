<?php
include_once ("../../vendor/autoload.php");

use Pondit\Institute\Group;
use Pondit\Institute\Mark;
use Pondit\Institute\Student;
use Pondit\Institute\Subject;
use Pondit\Institute\Teacher;

$group = new Group();
$mark = new Mark();
$student = new Student();
$subject = new Subject();
$teacher = new Teacher();
?>